const fs = require('fs');
const moment = require('moment')

let sateLiteInfos = []; // This will be an array of objects which contains each satelite reading per line
let resultArray = []; // This will be the the global output of the program

function read(filePath) {
    const readableStream = fs.createReadStream(filePath, 'utf8');
    readableStream.on('error', function (error) {
        console.log(`error: ${error.message}`);
    })
    readableStream.on('data', (chunk) => {
        let info = chunk.toString();
        processLines(info);        
    })
}

function processLines(dataStr) {
   //Break data stream chunk line by line, and store into linesArray
    const linesArray = dataStr.split("\n"); 
    //Iterate lines array, processing data into objects and store values in array
    for (let i=0; i < linesArray.length; i++){
        sateLiteInfos.push(processThisLine(linesArray[i]));
    }

    //Grouping battery and thermostat reading into separate arrays 
    const battVoltageReadings = sateLiteInfos.filter(obj => obj.component === 'BATT');
    const tstatVoltageReadings = sateLiteInfos.filter(obj => obj.component === 'TSTAT');

    // Setting the date initial dates for tstat and bstat readings.
    // Will be used when comparing all batt readings and tstat readings in their arrays, respectively 
    const minDateBatt = moment(battVoltageReadings[0].timestamp, moment.defaultFormat).toDate();
    const minDateTstat = moment(tstatVoltageReadings[0].timestamp, moment.defaultFormat).toDate();
    
    //Calls to compare Tstat readings and batt readings which are then pushed to result array
    compareGroupedTstatVoltageReadings(tstatVoltageReadings, minDateTstat);
    compareGroupedBattVoltageReadings(battVoltageReadings, minDateBatt);

    console.log(resultArray);
}

//Always compare current or next date to the first (prev);
function calcDifference(prevDate, currentDate){
    let res = prevDate - currentDate;
    res = Math.round((res/1000)/60);
    return res;
}

//Find three batvoltage reading for same satelite that exceed the red high limit within a five minute interval.
function compareGroupedTstatVoltageReadings(tstatVoltageArr, minDateTstat){
    let visited = new Set();
    for (let i = 0; i < tstatVoltageArr.length; i++){
        let currentObjItem = tstatVoltageArr[i];
        let count = 1;
    
        if (!visited.has(currentObjItem.satelliteId)){
            for (let j=i+1; j < tstatVoltageArr.length; j++){
                let nextObjItem = tstatVoltageArr[j];
                let nextDate = moment(nextObjItem.timestamp, moment.defaultFormat).toDate();
                
                    if (currentObjItem.satelliteId === nextObjItem.satelliteId
                         && (Number(nextObjItem.rawValue) > Number(nextObjItem.redHighLimit)) 
                         && (calcDifference(nextDate, minDateTstat) < 5)){ //if same Ids, and meets difference standards, increment count
                        count++;
                    }      
            }
        }
        visited.add(currentObjItem.satelliteId);
        if (count === 3){
            //Use same object in first loop where count totalled 3 to push new object with formatted fields. 
            const temp = {};
            temp["satelliteId"] = parseInt(currentObjItem.satelliteId);
            temp["severity"] = "RED HIGH";
            temp["component"] = currentObjItem.component;
            temp["timestamp"] = moment(currentObjItem.timestamp, moment.defaultFormat).toDate().toISOString();
            resultArray.push(temp);
            break;
        }
    }
}


//Find three batvoltage readings for same satelite that are under the red low limit within a five minute interval
function compareGroupedBattVoltageReadings(battVoltageArr, minDateBatt){
    let visited = new Set();
    for (let i = 0; i < battVoltageArr.length; i++){
        let currentObjItem = battVoltageArr[i];
        let count = 1;
    
        if (!visited.has(currentObjItem.satelliteId)){
            for (let j=i+1; j < battVoltageArr.length; j++){
                let nextObjItem = battVoltageArr[j];
                let nextDate = moment(nextObjItem.timestamp, moment.defaultFormat).toDate();
                
                    if (currentObjItem.satelliteId === nextObjItem.satelliteId &&
                        Number(nextObjItem.rawValue) < Number(nextObjItem.redLowLimit) && calcDifference(nextDate, minDateBatt) < 5){ //if same Ids, save the current, must be modified//and meets standards
                        count++;
                    }      
            }
        }
        visited.add(currentObjItem.satelliteId)
        if (count === 3){ //if count is 3 we have the desired warning, now push into result array

            const temp = {};
            temp["satelliteId"] = parseInt(currentObjItem.satelliteId);
            temp["severity"] = "RED LOW";
            temp["component"] = currentObjItem.component;
            temp["timestamp"] = moment(currentObjItem.timestamp, moment.defaultFormat).toDate().toISOString();
            resultArray.push(temp);
            break;
        }
    }
}


//Takes in a line as string and processes it into an object
function processThisLine(str){
    if (str.length > 0){
        let timestamp, satelliteID, redHigh, yellowHigh, yellowLow,
        redLow, rawValue, component  = "";
    
        timestamp = str.split("|")[0];
        satelliteID = str.split("|")[1];
        redHigh = str.split("|")[2];
        yellowHigh = str.split("|")[3];
        yellowLow = str.split("|")[4];
        redLow = str.split("|")[5];
        rawValue = str.split("|")[6];
        component = str.split("|")[7];
    
        return { 
                 "timestamp": timestamp,
                 "satelliteId": satelliteID,
                 "redHighLimit": redHigh,
                 "yellowHighLimit": yellowHigh,
                 "yellowLowLimit": yellowLow,
                 "redLowLimit": redLow,
                 "rawValue": rawValue,
                 "component": component            
               }
    }
}


read('example-input.txt');  